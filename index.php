<?php
/**
 * Created by PhpStorm.
 * User: Sabirzjanov
 * Date: 05.05.2015
 * Time: 17:43
 */


$edate = date("Y-m-d H:i:s", strtotime("2015-05-05 11:20:00"));
$mdate = date("Y-m-d H:i:s", strtotime("2015-05-06 11:20:00"));

$out = array();

 for($i=1; $i<=15; $i++){   //from day 01 to day 15
    $data = date('Y-m-d', strtotime("+".$i." days"));
    $out[] = array(
        'id' => $i,
        'title' => 'Event name '.$i,
        'description' => 'Описание события '.$i,
        'url' => 'www.example.com',
        'class' => 'event-success',
        'start' => strtotime($edate).'000'
    );
}

$params = array();
$params['{out}'] = json_encode($out);

echo strtr(file_get_contents('tmp.html'), $params);

//array('{edate}'=>strtotime($edate)."000", '{mdate}'=>strtotime($mdate)."000")